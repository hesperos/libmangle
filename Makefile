TARGET=libmangle.a
SOURCES= \
		gc.c \
		xtea.c \
		serializer.c

COBJ=$(SOURCES:.c=.o)
CC=gcc
AR=ar
RANLIB=ranlib
STRIP=strip
CFLAGS=-I. -Wall -O2 -ffunction-sections -fdata-sections
LDFLAGS=

all: $(TARGET)

%.o: %.c
	@echo -e "\tCC" $<
	@$(CC) -c -o $@ $< $(CFLAGS)

$(TARGET): $(COBJ)
	@echo -e "\tARCHIVING CC" $(COBJ)
	$(STRIP) -g $(COBJ)
	$(AR) rcsv $(TARGET) $(COBJ)
	$(RANLIB) $(TARGET)

clean:
	@echo ========== cleanup ========== 
	rm -rf *.s *.o $(TARGET)	
