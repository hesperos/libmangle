#ifndef __GC_H__
#define __GC_H__


/**
 * @brief add pointer to be garbage collected
 *
 * @param a_ptr
 */
void mangle_gc_add_ptr(void *a_ptr);


/**
 * @brief garbage collector
 */
void mangle_gc(void);


#endif /* __GC_H__ */
