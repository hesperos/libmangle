#include "xtea.h"
#include "serializer.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>


#define NUM_ROUNDS 2


/**
 * @brief delta constant for xtea algorithm
 */
static const uint32_t delta=0x9E3779B9;


/**
 * @brief default key definition
 */
xtea_key_t xtea_default_key = {0x00};


/**
 * @brief xtea encoder
 *
 * @param num_rounds number of rounds to perform (the more, the better)
 * @param v data block to encode
 * @param key key
 */
static void _xtea_encipher(uint32_t num_rounds, xtea_blk_t v, xtea_key_t key) {
    uint32_t v0=v[0], v1=v[1], sum=0;
	while (num_rounds--) {
        v0 += (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + key[sum & 3]);
        sum += delta;
        v1 += (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + key[(sum>>11) & 3]);
    }
    v[0]=v0; v[1]=v1;
}

 
/**
 * @brief xtea decoder
 *
 * @param num_rounds number of rounds to perform (the more, the better)
 * @param v data block to decode
 * @param key key
 */
static void _xtea_decipher(uint32_t num_rounds, xtea_blk_t v, xtea_key_t key) {
    uint32_t v0=v[0], v1=v[1], sum=delta*num_rounds;
    while (num_rounds--) {
        v1 -= (((v0 << 4) ^ (v0 >> 5)) + v0) ^ (sum + key[(sum>>11) & 3]);
        sum -= delta;
        v0 -= (((v1 << 4) ^ (v1 >> 5)) + v1) ^ (sum + key[sum & 3]);
    }
    v[0]=v0; v[1]=v1;
}



char* mangle_xtea_dec(char* a_str, xtea_key_t a_key) {
	char *ptr = a_str;
	size_t s = 
		mangle_deserialize_get_size(strlen(a_str))/sizeof(xtea_blk_t) + 1;

	mangle_deserialize_str(a_str, a_str);

	while (s--) {
		_xtea_decipher(NUM_ROUNDS, (uint32_t *)ptr, a_key);
		ptr += sizeof(xtea_blk_t);
	}

	return a_str;
}

 
char* mangle_xtea_dec_const(const char* a_str, xtea_key_t a_key) {
	size_t s = strlen(a_str)/sizeof(xtea_blk_t) + 1;
	char *deserialized = NULL;
	char *ptr = NULL;

	if (!strlen(a_str)) {
		return NULL;
	}

	deserialized = mangle_deserialize_str_const(a_str);
	if (!deserialized) {
		return NULL;
	}
	
	ptr = deserialized;
	while (s--) {
		_xtea_decipher(NUM_ROUNDS, (uint32_t *)ptr, a_key);
		ptr += sizeof(xtea_blk_t);
	}

	return deserialized;
}
 

char* mangle_xtea_encode(char* a_str, xtea_key_t a_key) {
	size_t s = strlen(a_str)/sizeof(xtea_blk_t) + 1;
	char *ptr = a_str;
	char *sd = NULL;

	while (s--) {
		// xtea operates on blocks
		// the buffer's size must be multples of the xtea_blk_t size
		_xtea_encipher(NUM_ROUNDS, (uint32_t *)ptr, a_key);
		ptr += sizeof(xtea_blk_t);
	}

	// serialize and add node to garbage collector list
	sd = mangle_serialize(a_str, (size_t)(ptr - a_str));
	return sd;
}



void mangle_xtea_set_key(const xtea_key_t a_key) {
	memcpy(xtea_default_key, a_key, sizeof(xtea_key_t));
}


inline size_t mangle_xtea_get_size(size_t s) {
	// the size is increased to two blocks - just as a safety precaution
	return ( ((s/sizeof(xtea_blk_t)) + 2) * sizeof(xtea_blk_t) );
}
