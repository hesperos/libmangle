#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "serializer.h"
#include "gc.h"

#undef CHECK_FOR_BLOCK_ALIGNMENT
#define OFFSET_CONST 0x32

#define BLOCK_SIZE_8BIT 3
#define BLOCK_SIZE_6BIT 4


/**
 * @brief serialize high nibble
 */
static inline uint8_t _sh(uint8_t *data, uint8_t i) {
	return (*data >> (8 - (i << 1)));
}


/**
 * @brief serialize low nibble
 */
static inline uint8_t _sl(uint8_t *data, uint8_t i) {
	return ((*data << (i << 1)) & 0x3f);
}


/**
 * @brief de-serialize high nibble
 */
static inline uint8_t _dh(uint8_t *data, uint8_t i) {
	return ( *data >> (i << 1) );
}


/**
 * @brief de-serialize low nibble
 */
static inline uint8_t _dl(uint8_t *data, uint8_t i) {
	return ( *data << (6 - (i << 1)) );
}


/**
 * @brief serialize data stream of given length
 *
 * @param a_data data
 * @param is size
 * @param a_output output buffer
 */
static void _serialize(char *a_data, int is, char *a_output) {
	uint8_t i = 0;

    /*
	 * block version:
	 *
	 * *a_output++ = _snh(a_data, 0) | _snl(a_data, 0);
	 * *a_output++ = _snh(a_data, 1) | _snl(++a_data, 1);
	 * *a_output++ = _snh(a_data, 2) | _snl(++a_data, 2);
	 * *a_output++ = _snh(a_data, 3) | _snl(a_data, 3);
     */
	
	while (is--) {
		if (!i) *a_output = _sh((uint8_t *)a_data, i);
		*a_output++ |= _sl((uint8_t *)a_data, i);
		*a_output = _sh((uint8_t *)a_data++, i + 1);
		if(i == 2) a_output++;
		i = (i + 1) % BLOCK_SIZE_8BIT;
	}
}


/**
 * @brief add a constant offset to the serialized stream
 *
 * @param a_data data
 * @param is data size
 * @param offset offset value
 */
static inline void _add_offset(uint8_t *a_data, size_t is, uint8_t offset) {
	while (is--) {
		*a_data += offset;
		switch(*a_data) {
			case '\\':
				*a_data = 'z';
				break;
		}
		a_data++;
	}
}


/**
 * @brief remove an offset from serialized string
 *
 * @param a_data data
 * @param is serialized string length
 * @param offset offset value
 */
static inline void _rem_offset(uint8_t *a_data, size_t is, uint8_t offset) {
	while (is--) {
		switch(*a_data) {
			case 'z':
				*a_data = '\\';
				break;
		}
		*a_data -= offset;
		a_data++;
	}
}


char* mangle_serialize(char *a_data, size_t is) {
	size_t oblks = mangle_serialize_get_size(is);

	// need one extra for terminating NUL byte
	void *os = malloc(oblks + 1);

	if (os) {
		mangle_gc_add_ptr(os);
		memset(os, 0x00, oblks + 1);
		_serialize(a_data, is, os);
		_add_offset(os, oblks, OFFSET_CONST);
	}
	return os;
}


char* mangle_deserialize(char *a_sdata, size_t is, char *a_output) {
	uint8_t i = 0;
	char *rv = a_output;

#ifdef CHECK_FOR_BLOCK_ALIGNMENT
    /*
	 * Output buffer's size must be multiples of 4
	 * if it's not - abort. Most likely this is not
	 * a serialized string
     */
	if (is % BLOCK_SIZE_6BIT) {
		fprintf(stderr, "%s(): invalid input buffer size [%d]\n", 
				__FUNCTION__,
				is);
		return NULL;
	}
#endif

    /*
	 * copy input to output only if addresses aren't pointing
	 * to the same buffer
     */
	if (a_sdata != a_output) 
		memcpy(a_output, a_sdata, is);

	/* remove offset first */
	_rem_offset((uint8_t *)a_output, is, OFFSET_CONST);
	a_sdata = a_output;

    /*
	 * block version:
	 *
	 * *a_output++ = _dnh(a_sdata, 0) | _dnl(++a_sdata, 0);
	 * *a_output++ = _dnh(a_sdata, 1) | _dnl(++a_sdata, 1);
	 * *a_output++ = _dnh(a_sdata, 2) | _dnl(++a_sdata, 2);
     */

	while (is--) {
		if (i == 0) {
			*a_output = _dh((uint8_t *)a_sdata, i);
		}
		else {
			*a_output++ |= _dl((uint8_t *)a_sdata, i - 1);
			if (i != 3) *a_output = _dh((uint8_t *)a_sdata, i);
		}
		a_sdata++;
		i = (i + 1) % BLOCK_SIZE_6BIT;
	}

	return rv;
}


void* mangle_deserialize_const(const char* a_sdata, size_t is) {
	void *os = malloc(strlen(a_sdata) + 1);
		
	if (os) {
		mangle_gc_add_ptr(os);
		strcpy(os, a_sdata);
		mangle_deserialize(os, strlen(os), os);
	}
	return os;
}


char* mangle_deserialize_str(char *a_sdata, char *a_output) {
	return mangle_deserialize(a_sdata, strlen(a_sdata), a_output);
}


char* mangle_deserialize_str_const(const char *a_sdata) {
	return mangle_deserialize_const(a_sdata, strlen(a_sdata));
}


inline size_t mangle_serialize_get_size(size_t a_size) {
	size_t blks = (a_size/BLOCK_SIZE_8BIT);
	return ( (blks + 1)*BLOCK_SIZE_6BIT );
}


inline size_t mangle_deserialize_get_size(size_t a_size) {
	size_t blks = (a_size/BLOCK_SIZE_6BIT);
	return ( (blks - 1)*BLOCK_SIZE_8BIT );
}

