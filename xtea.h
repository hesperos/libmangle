#ifndef XTEA_H_OX1SJNG4
#define XTEA_H_OX1SJNG4

#include <stdint.h>		
#include <stdlib.h>


/**
 * @brief single data block type
 */
typedef uint32_t xtea_blk_t[2];


/**
 * @brief key type definition
 */
typedef uint32_t xtea_key_t[4];


/**
 * @brief default key
 */
extern xtea_key_t xtea_default_key;


/**
 * @brief encode the string in place
 *
 * @param a_str string to be encoded
 * @param a_key key
 */
char* mangle_xtea_encode(char* a_str, xtea_key_t a_key);


/**
 * @brief decode the string in-place
 *
 * @param a_str encoded string to be decoded
 * @param a_key key
 *
 * @return pointer to a_str
 */
char* mangle_xtea_dec(char* a_str, xtea_key_t a_key);


/**
 * @brief decode a const string 
 *
 * @param a_str encoded string
 * @param a_key key
 *
 * @return heap allocated decoded string (will be freed by the garbage collector)
 */
char* mangle_xtea_dec_const(const char* a_str, xtea_key_t a_key);


/**
 * @brief configure the default key
 *
 * @param a_key key value
 */
void mangle_xtea_set_key(const xtea_key_t a_key);


/**
 * @brief given the size, calculate the size (in bytes) of xtea encoded data
 *
 * @param s unencoded data size
 *
 * @return encoded data size (in bytes)
 */
size_t mangle_xtea_get_size(size_t s);

#endif /* end of include guard: XTEA_H_OX1SJNG4 */
