#ifndef MANGLE_H_MTO1JT8W
#define MANGLE_H_MTO1JT8W

/* Copyright (C) 
 * 2014 - Tomasz Wisniewski
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

/**
 * @mainpage Introduction
 *
 * libmangle can be used to obfuscate and serialize static data in C programs.
 * There are several situation when it may become quite handy:
 * - obfuscation of statically declared strings
 * - object serialization
 * - data serialization
 *
 * @section structure Structure
 *
 * The library is very small and very simple. It's build up out of three very simple
 * components
 * - encoder (obfuscator)
 * - serializer
 * - garbage collector
 *
 * @section encoder Encoder
 *
 * The encoder implements the XTEA (block Tiny Encryption Algorithm) cipher. It's not meant
 * to be ultra cryptographically strong, but more of to make eventual attack more complex
 * and time consuming. 
 *
 * The encoder encrypts the given block of data and serializes it.
 *
 * Please refer to the documentation for xtea.h file for more details.
 *
 * API
 * - mangle_xtea_encode()
 * - mangle_xtea_dec()
 * - mangle_xtea_dec_const()
 * - mangle_xtea_set_key()
 * - mangle_xtea_get_size()
 *
 * @section serializer Serializer
 *
 * The encoder returns a stream of binary data. Such data may occur to be unusable as strings. Since
 * not every byte can be printed, transfered via serial terminals etc. The serializer takes the stream
 * of 8 bit bytes and converts it to a printable stream of 6 bit bytes.
 *
 * Example:
 *
 * Having a stream of 3 bytes: 0xff, 0xff, 0xff. The serializer will distribute this data into
 * 4 6 bit bytes: 0x3f, 0x3f, 0x3f, 0x3f. Those values can be easilly printed on most ASCII terminals.
 *
 * Please refer to the documentation for serializer.h file for more details.
 *
 * API
 * - mangle_serialize()
 * - mangle_deserialize()
 * - mangle_deserialize_const()
 * - mangle_deserialize_str()
 * - mangle_deserialize_str_const()
 * - mangle_serialize_get_size()
 * - mangle_deserialize_get_size()
 *
 * @section gc Garbage Collector
 *
 * The output data from the serializer is heap-allocated. Every pointer created by the library
 * is collected and tracked by the garbage collector. In order to free all the allocations
 * done internally by the library. Simply call mangle_gc().
 *
 * API
 * - mangle_gc() - call garbage collection (should be done at the end of your program)
 *
 * Please refer to the documentation for gc.h file for more details.
 *
 * @section usage Usage
 *
 * Most of the usecase or covered in examples. Look them up as a reference in order
 * to get a general understanding.
 *
 * - basic.c - example of basic usage
 * - serialization.c - using libmangle to serialize objects
 *
 * @example basic.c
 * @example serialization.c
 *
 */




#include "serializer.h"
#include "gc.h"
#include "xtea.h"

#endif /* end of include guard: MANGLE_H_MTO1JT8W */
