#ifndef SERIALIZE_H_JQ2TMDO0
#define SERIALIZE_H_JQ2TMDO0

#include <stdlib.h>

/**
 * @brief serialize the binary data to 6-bit ASCII stream
 *
 * @param a_data input data
 * @param is input data size
 *
 * @return pointer to allocated buffer containing serialized data. Should be
 * freed by the user
 */
char* mangle_serialize(char *a_data, size_t is);


/**
 * @brief deserialize data in the given buffer
 *
 * @param a_sdata input buffer
 * @param is serialized data string length (must be multiples of 4)
 * @param a_output output buffer (may be the same as input buffer)
 *
 * @return pointer to the output buffer
 */
char* mangle_deserialize(char *a_sdata, size_t is, char *a_output);


/**
 * @brief deserializes the const string
 *
 * @param a_sdata input stream
 * @param is input stream length (must be multiples of 4)
 *
 * @return pointer to allocated buffer containing binary data. Should be freed
 * by the user
 */
void* mangle_deserialize_const(const char* a_sdata, size_t is);


/**
 * @brief wrapper for deserialize, does strlen(a_sdata) to determine the length
 *
 * @param a_sdata serialized stream
 * @param a_output output buffer
 *
 * @return output buffer
 */
char* mangle_deserialize_str(char *a_sdata, char *a_output);


/**
 * @brief wrapper for deserialize_const, does strlen(a_sdata) to determine the length
 *
 * @param a_sdata input stream
 *
 * @return pointer to allocated buffer containing binary data. Should be freed
 * by the user
 */
char* mangle_deserialize_str_const(const char *a_sdata);


/**
 * @brief calculates the length of the serialized string 
 *
 * @param a_size size of unserialized data
 *
 * @return length of serialized string
 */
size_t mangle_serialize_get_size(size_t a_size);


/**
 * @brief calculates the rounded length of the de-serialized string
 *
 * @param a_size serialized string length
 *
 * @return deserialized string length
 */
size_t mangle_deserialize_get_size(size_t a_size);

#endif /* end of include guard: SERIALIZE_H_JQ2TMDO0 */
