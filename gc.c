#include "gc.h"

#include <stdlib.h>
#include <stdio.h>


/**
 * @brief node type to hold the memory pointers allocated
 */
struct node {
	struct node* _next;
	void *ptr;
};


/**
 * @brief linked list's head
 */
static struct node* m_nodes = NULL;


/**
 * @brief append pointer to the list
 *
 * @param h current list head
 * @param ptr pointer
 *
 * @return new list head
 */
static struct node* _node_append(struct node* h, void *ptr) {
	struct node *nn = malloc(sizeof(struct node));
	if (!nn) {
		fprintf(stderr, "%s(): no memory", __FUNCTION__);
		return h;
	}
	nn->ptr = ptr;
	nn->_next = h;
	return nn;
}


void mangle_gc_add_ptr(void *a_ptr) {
	m_nodes = _node_append(m_nodes, a_ptr);
}


void mangle_gc() {
	struct node* ptr = m_nodes;
	while (ptr) {
		struct node* n = ptr->_next;
		if (ptr->ptr) free(ptr->ptr);
		free(ptr);
		ptr = n;
	}
}

