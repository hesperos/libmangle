# libmangle

 libmangle can be used to obfuscate and serialize static data in C programs.
 
 There are several situation when it may become quite handy:
 - obfuscation of statically declared strings
 - object serialization
 - data serialization

 Detailed description is available in Doxygen:

[ libmangle doxygen ](http://dagon666.github.io/libmangle/)
