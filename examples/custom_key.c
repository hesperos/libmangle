#include <mangle.h>

#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	xtea_key_t key = { 0x01, 0x02, 0x03, 0x04 };
	char s1[128] = "this string will be encoded and serialized";
	char *str = mangle_xtea_encode(s1, key);
	
	printf("encoded: [%s]\n", str);

	memset(s1, 0x00, sizeof(s1));
	mangle_xtea_dec(str, key);
	printf("decoded: [%s]\n", str);

	mangle_gc();
	return 0;
}
