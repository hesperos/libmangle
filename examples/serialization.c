#include <mangle.h>

#include <stdio.h>
#include <string.h>

struct data {
	int x,y,z;
};


int main(int argc, char *argv[]) {
	struct data a = { 1, 2, 3 };
	char *str = mangle_serialize((char *)&a, sizeof(a));
	
	printf("serialized: [%s]\n", str);

	memset(&a, 0x00, sizeof(a));
	mangle_deserialize_str(str, (char *)&a);

	printf("x: %d, y: %d, z: %d\n", 
			a.x,
			a.y,
			a.z);

	mangle_gc();
	return 0;
}
